.. image:: https://semaphoreci.com/api/v1/projects/07e2137a-015b-4b08-9a1c-e20a1785f373/1656514/badge.svg
    :target: https://semaphoreci.com/goromasamune/test_repo

.. image:: https://img.shields.io/badge/license-MIT-blue.svg
    :target: https://bitbucket.org/gobrewers14/test_repo/src/45f04aea226904391dabcb40abd506d9b990d8c7/LICENSE?at=master&fileviewer=file-view-default

.. image:: https://coveralls.io/repos/bitbucket/gobrewers14/test_repo/badge.svg?branch=master
    :target: https://coveralls.io/bitbucket/gobrewers14/test_repo?branch=master


=========================================
IPython: Productive Interactive Computing
=========================================

Overview
========

Welcome to IPython.  Out full documentation is available on `our website
<http://ipython.org/documentation.html>`_; if you downloaded a build source
distribution the ``docs/source`` directory contains the plaintext version of
these manuals.

Instant running
===============

You can run IPython from this directory without even installing it system-wide
by typing at the termina::

    python -m IPython
