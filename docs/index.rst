.. test_repo documentation master file, created by
   sphinx-quickstart on Wed Oct  4 09:01:17 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to test_repo's documentation!
=====================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

.. include:: ../README.rst

Read More
---------

.. toctree::
    :maxdepth: 2

    example
