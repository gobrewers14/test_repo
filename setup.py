from setuptools import setup
from setuptools import find_packages
import os

here = os.path.dirname(os.path.abspath(__file__))


setup(
    name="test_repo",
    version=0.1,
    author="john martinez",
    author_email="john.martinez@toyotaconnected.com",
    license="MIT",
    include_package_data=False,
    zip_safe=False,
    packages=find_packages(),
)
