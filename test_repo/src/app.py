# -*- coding: utf-8 -*-

def a_function_to_test(x):
    """
    Parameters
    ----------
    x : int
      a python integer

    Returns
    -------
    output : bool
       whether x is bigger or smaller than 5
    """
    if x > 5:
        return True

    return False
