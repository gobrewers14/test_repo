def my_func(arg):
    """
    this function does something

    >>> from example import my_func
    >>> my_func(
    ...    "hi"
    )
    True

    :param arg: a python str - the argument
    :returns: bool - the value
    """
    return True
